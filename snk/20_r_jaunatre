####################
configfile: 'config.yaml'

####################
from snakemake.utils import R

####################
def get_factor_list(wildcards):
    return config['jaunatre']['plotV2']['factor'][wildcards]

def get_ref_color(wildcards):
     return config['jaunatre']['plotV2']['colref'][wildcards]

def get_miss_color(wildcards):
     return config['jaunatre']['plotV2']['colmiss'][wildcards]

def get_test_color(wildcards):
     return config['jaunatre']['plotV2']['coltest'][wildcards]

def get_higher_color(wildcards):
     return config['jaunatre']['plotV2']['colhigher'][wildcards]

def get_exp_name(wildcards):
     return int(wildcards) / 13

####################
rule all:
    input:
        INDICES = expand('out/{data}/{primer}/jaunatre/{filt}/Indices-{assess}-vs-{reference}-with-rar-{rar}.tsv',
                         data = config['datasets']['data'],
                         primer = config['datasets']['primer'],
                         filt = config['datasets']['filter']['both'],
                         assess = config['jaunatre']['indices']['assess'],
                         reference = config['jaunatre']['indices']['reference'],
                         rar = config['jaunatre']['indices']['rar']),

        PLOT = expand('out/{data}/{primer}/jaunatre/{filt}/Plot-{assess}-vs-{reference}-with-rar-{rar}.pdf',
                      data = config['datasets']['data'],
                      primer = config['datasets']['primer'],
                      filt = config['datasets']['filter']['both'],
                      assess = config['jaunatre']['indices']['assess'],
                      reference = config['jaunatre']['indices']['reference'],
                      rar = config['jaunatre']['indices']['rar'])
       
####################
rule JAUNATRE:
    #Aim: Calculates indices of community integrity compared to a reference community according to Jaunatre et al. (2013).
    #Use: "ComSII" R package --> 'ComStructIndices' and 'structure.plotV2' functions.
    log:
    params:
        mirror = config['r']['mirror'],
        factor = lambda wildcards: get_factor_list(wildcards.primer),
        multi = config['jaunatre']['plotV2']['multi'],
        maxabundance = config['jaunatre']['plotV2']['maxabundance'],
        colref = lambda wildcards: get_ref_color(wildcards.reference),
        colmiss = lambda wildcards: get_miss_color(wildcards.reference),
        coltest = lambda wildcards: get_test_color(wildcards.reference),
        colhigher = lambda wildcards: get_higher_color(wildcards.reference),
        names = config['jaunatre']['plotV2']['names'],
        namesexp = lambda wildcards: get_exp_name(wildcards.rar),
        error = config['jaunatre']['plotV2']['error'],
        adjmeth = config['jaunatre']['plotV2']['adjmeth'],
        errorbarwidth = config['jaunatre']['plotV2']['errorbarwidth'],
        stars = config['jaunatre']['plotV2']['stars'],
        spstar = config['jaunatre']['plotV2']['spstar'],
        base = config['jaunatre']['plotV2']['base']
    input:
        REF = 'out/{data}/{primer}/qiime2/export/subtables/{reference}-{filt,.*}Table/ESV.tsv',
        ASSESS = 'out/{data}/{primer}/qiime2/export/subtables/{assess}-{filt,.*}Table/ESV.tsv'
    output:
        INDICES = 'out/{data}/{primer}/jaunatre/{filt,.*}/Indices-{assess}-vs-{reference}-with-rar-{rar}.tsv',
        PDF = 'out/{data}/{primer}/jaunatre/{filt,.*}/Plot-{assess}-vs-{reference}-with-rar-{rar}.pdf',
        PNG = 'out/{data}/{primer}/jaunatre/{filt,.*}/Plot-{assess}-vs-{reference}-with-rar-{rar}.png'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("devtools")) {{install.packages("devtools", repos="{params.mirror}")}}
            if (!require("ComSII")) {{install_github("RenaudJau/ComSII")}}
            #
            ### LOAD LIBRARIES
            library(devtools)
            library(ComSII)
            #
            ### LOAD REFERENCES AND ASSESS TABLES
            REF <- as.data.frame(t(read.delim("{input.REF}", sep = "\t", dec = ".", row.names = 1, header = TRUE)))
            ASSESS <- as.data.frame(t(read.delim("{input.ASSESS}", sep = "\t", dec = ".", row.names = 1, header = TRUE)))
            #
            ### SAVE INDICES AS TABLE
            DATA <- ComStructIndices(REF, ASSESS, rar = {wildcards.rar})
            INDICES <- as.data.frame(DATA$CSII)
            INDICES[,2] <- as.data.frame(DATA$CSIInorm)
            INDICES[,3] <- as.data.frame(DATA$HAI)
            write.table(INDICES, file = "{output.INDICES}", sep = "\t")
            #
            ### PLOT
            #
            pdf("{output.PDF}")                          # save as .pdf
            structure.plotV2(DATA,                       # An object issued from 'ComStructIndices' function.
                 FACTOR = as.factor(c({params.factor})), # A factor list, a barplot of species mean abundances will be performed for each fact lvl.
                 MULTI = {params.multi},                 # If no factor is specified, OR, if not enough 'y' observationsMULTI = F should be specified.
                 MTITLE = "Assess: {wildcards.assess} vs Ref: {wildcards.reference} with rar={wildcards.rar}", # Main title of the plot.
                 ABMAX = {params.maxabundance},          # Numerical value of the maximum abundance.
                 col1 = "{params.colref}",               # Colour information for the Reference mean abundances barplot.
                 col2 = "{params.colmiss}",              # Colour information in assesssed community barplot, i.e. "missing abundances".
                 col3 = "{params.coltest}",              # Colour information for the abundances of target species in the assesssed community.
                 col4 = "{params.colhigher}",            # Colour information for the "higher abundances" in the assessed community.
                 noms = "{params.names}",                # If other than "T", species names are not given.
                 cex_noms = {params.namesexp},           # Expansion factor for species names.
                 erreur = "{params.error}",              # Error calculation (facultative).
                 w_err = {params.errorbarwidth},         # Error bar width.
                 sp_star = {params.spstar},              # Width between stars and error bars.
                 adj_meth = "{params.adjmeth}",          # p adjust method: "bond","BH","hoch" or "none" (p: wilcoxon-t between ref. and mod.).
                 stars = "{params.stars}",               # Default = "T", draw or note the stars, anything but "T" does not draw the stars.
                 BASE = {params.base})                   # Default = T, draw or not the reference community for each modality.
                 #...                                    #...: any arguments from a 'barplot' function can be used.
            dev.off()                                    # Close saving
            #
            png("{output.PNG}")                          # save as .png
            structure.plotV2(DATA,                       # An object issued from 'ComStructIndices' function.
                 FACTOR = as.factor(c({params.factor})), # A factor list, a barplot of species mean abundances will be performed for each fact lvl.
                 MULTI = {params.multi},                 # If no factor is specified, OR, if not enough 'y' observationsMULTI = F should be specified.
                 MTITLE = "Assess: {wildcards.assess} vs Ref: {wildcards.reference} with rar={wildcards.rar}", # Main title of the plot.
                 ABMAX = {params.maxabundance},          # Numerical value of the maximum abundance.
                 col1 = "{params.colref}",               # Colour information for the Reference mean abundances barplot.
                 col2 = "{params.colmiss}",              # Colour information in assesssed community barplot, i.e. "missing abundances".
                 col3 = "{params.coltest}",              # Colour information for the abundances of target species in the assesssed community.
                 col4 = "{params.colhigher}",            # Colour information for the "higher abundances" in the assessed community.
                 noms = "{params.names}",                # If other than "T", species names are not given.
                 cex_noms = {params.namesexp},           # Expansion factor for species names.
                 erreur = "{params.error}",              # Error calculation (facultative).
                 w_err = {params.errorbarwidth},         # Error bar width.
                 sp_star = {params.spstar},              # Width between stars and error bars.
                 adj_meth = "{params.adjmeth}",          # p adjust method: "bond","BH","hoch" or "none" (p: wilcoxon-t between ref. and mod.).
                 stars = "{params.stars}",               # Default = "T", draw or note the stars, anything but "T" does not draw the stars.
                 BASE = {params.base})                   # Default = T, draw or not the reference community for each modality.
                 #...                                    #...: any arguments from a 'barplot' function can be used.
            dev.off()                            # Close saving
            #
            # Details: Be careful, the p adjustment is done only within a modality.
            # Perhaps that would be better to adjust it through all the n modalitties (to be modified later).
            # The warnings come from ties, but it seems it does not change the outcome of tests...
            #
            """)
